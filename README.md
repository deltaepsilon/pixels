xScope works amazingly well on Mac, but I'm left high and dry when I switch to Windows. What if the core xScope functionality for pixel matching mocks was baked into Chrome via an extension?

### Resources

[Chrome extension tutorial](https://developer.chrome.com/extensions/getstarted)

[Devtools extension tutorial](https://developer.chrome.com/extensions/devtools#examples)

[Firebase Chrome Extension tutorial](https://github.com/firebase/quickstart-js/tree/master/auth/chromextension)

### Links

[API Credentials](https://console.developers.google.com/apis/credentials/consent?project=quiver-pixels&authuser=1&folder&organizationId=997564306005)

## Deploy

[Set up deploy](https://dev.to/gokatz/automate-your-chrome-extension-deployment-in-minutes-48gb)
[Obtain keys](https://github.com/DrewML/chrome-webstore-upload/blob/master/How%20to%20generate%20Google%20API%20keys.md)
