const fs = require('fs');
const webStore = require('./utilities/webstore');

const zipName = '../app/extension/dist.zip';

async function upload() {
  const source = fs.createReadStream(zipName);

  return webStore.uploadExisting(source);
}

(async () => {
  try {
    const result = await upload();

    console.info('Successfully uploaded the ZIP', result);
    process.exit();
  } catch (error) {
    console.info(`Error while uploading ZIP: ${error}`);
    process.exit(1);
  }
})();
