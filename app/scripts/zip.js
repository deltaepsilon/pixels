const fs = require('fs');

function zip() {
  const zipFolder = require('zip-folder');
  const folderName = '../app/extension/dist';
  const zipName = '../app/extension/dist.zip';

  return new Promise((resolve, reject) =>
    zipFolder(folderName, zipName, err => (err ? reject(err) : resolve(zipName)))
  );
}

(async () => {
  const zipName = await zip();

  fs.copyFileSync(zipName, '../app/assets/public/pixels.zip')

  console.info(`Successfully zipped ${zipName}`);
})();
