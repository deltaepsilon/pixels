const { CLIENT_ID, CLIENT_SECRET, EXTENSION_ID, REFRESH_TOKEN } = process.env;

const webStore = require('chrome-webstore-upload')({
  extensionId: EXTENSION_ID,
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  refreshToken: REFRESH_TOKEN,
});

module.exports = webStore;
