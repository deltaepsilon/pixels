const webStore = require('./utilities/webstore');

(async () => {
  try {
    const result = await webStore.publish();

    console.info('Successfully published', result);
    process.exit();
  } catch (error) {
    console.info(`Error while publishing: ${error}`);
    process.exit(1);
  }
})();
