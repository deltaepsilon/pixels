## Privacy Policy

### What information do I collect?

The only personally-identifiable information that this application collects is your **email address**.

It only collects your email if you sign in with Google. Guest sign in is completely anonymous.

### How do I use the information?

I don't use your information at the moment. I may send you an email in the future. If I do decide to **send email**, it will be via [MailChimp](https://mailchimp.com/) and will contain a one-click opt-out link.

### What information do I share?

I may share your email with **MailChimp**. I won't share it with anyone else without updating this privacy policy. And if I do need to update the privacy policy, that will be a great time to actually send you an email :)
