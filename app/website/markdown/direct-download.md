## Direct Download

I've been having a terrible time getting the Chrome Web Store to publish this extension correctly.

The solution is to publish the chrome extension for direct download.

You can drag-and-drop [pixels.zip](/assets/pixels.zip) directly to your Chrome extensions page.

Manually enter this URL into a new tab: **chrome://extensions**

Somehow this drag-and-drop install resolves the permissions problems that keep the normal install from working.
