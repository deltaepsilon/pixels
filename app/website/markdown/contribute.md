## Contribute

The code is hosted on [GitLab](https://gitlab.com/deltaepsilon/pixels). Go check it out, fork it, modify it to your heart's content... and let me know how it goes! 

[![GitLab icon](/assets/gitlab-icon-rgb.svg)](https://gitlab.com/deltaepsilon/pixels)
