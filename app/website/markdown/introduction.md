## Why Pixels?

My name is Chris Esplin, and I'm a Front-End Web Developer.

I've matched many mocks in my day, and I like to match them to the pixel. It respects the original design, and it makes my job easier. If the designer doesn't like it, the designer can give me better mocks. My pixel-matched designs should be practically indistinguishable from the mocks that I've received.

## How do I pixel-match with Pixels?

Pixels packages the tools that you need in a browser extension. 

Here's what you'll need to do:

1. Drag and drop your mocks into new browser tabs. 
2. Drop guidelines on your mocks. 
3. View those guidelines on your development website.
4. Tweak the CSS to your heart's content.
5. Profit. Seriously. People pay good money for pixel-perfect websites.
