## Terms of Service

I don't charge anything, so you don't get any special guarantees. I'll be honest and not mess with your browser. That's about it.

Please don't repackage and sell this app just because the code is open source. That would be lame. Please email me at [chris@chrisesplin.com](mailto:chris@chrisesplin.com) if you're interested in re-purposing my code in any way.