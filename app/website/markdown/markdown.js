import React from 'react';
import MarkdownIt from 'markdown-it';

import './markdown.css';

const md = new MarkdownIt({
  html: true,
});

export default class Markdown extends React.Component {
  render() {
    const { markdown } = this.props;
    let __html = md.render(markdown);

    return (
      <div className="markdown" style={{ padding: '1rem 2rem' }}>
        <div dangerouslySetInnerHTML={{ __html }} />
      </div>
    );
  }
}
