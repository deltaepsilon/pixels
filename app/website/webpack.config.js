const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    app: './website/app.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/, /background\.js/],
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.md$/,
        use: [{ loader: 'html-loader' }],
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './website/index.html', to: 'index.html' },
      { from: './website/sw.js', to: 'sw.js' },
      { from: './website/manifest.webmanifest', to: 'manifest.webmanifest' },
      { from: './assets/public', to: 'assets' },
    ]),
  ],
  devServer: {
    contentBase: path.join(__dirname),
  },
};
