import React from 'react';
import ReactDom from 'react-dom';
import Markdown from './markdown/markdown';
import introductionMarkdown from './markdown/introduction.md';
import directDownloadMarkdown from './markdown/direct-download.md';
import privacyPolicyMarkdown from './markdown/privacy-policy.md';
import termsOfServiceMarkdown from './markdown/terms-of-service.md';
import contributeMarkdown from './markdown/contribute.md';

import { Button } from '@rmwc/button';

import '@material/button/dist/mdc.button.css';

import '../root.css';
import './app.css';

const chromeWebstoreUrl =
  'https://chrome.google.com/webstore/detail/pixels/ibjfclodnfhjfighgdglngeaepomnamk';

function App(props) {
  return (
    <div id="app">
      <div className="introduction">
        <h1>Pixels</h1>

        <p>Do justice to your designs.</p>

        <img src="/assets/logo-168.png" />

        <div className="cta">
          <a href={chromeWebstoreUrl}>
            <Button raised>Available on the Chrome Web Store</Button>
          </a>
        </div>
      </div>

      <Markdown markdown={directDownloadMarkdown} />

      <div className="cta">
        <a href="/assets/pixels.zip">
          <Button raised>Download Pixels.zip</Button>
        </a>
      </div>

      <Markdown markdown={introductionMarkdown} />
      <Markdown markdown={privacyPolicyMarkdown} />
      <Markdown markdown={termsOfServiceMarkdown} />
      <Markdown markdown={contributeMarkdown} />
    </div>
  );
}

ReactDom.render(<App />, document.getElementById('root'));
