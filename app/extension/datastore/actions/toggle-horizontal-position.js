import setStorage from '../../utilities/set-storage';
import constants from '../../constants';

export default async ({ horizontalPosition }) =>
  setStorage({
    horizontalPosition:
      horizontalPosition == constants.POSITIONS.BOTTOM
        ? constants.POSITIONS.TOP
        : constants.POSITIONS.BOTTOM,
  });
