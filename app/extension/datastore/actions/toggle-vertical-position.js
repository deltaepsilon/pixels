import setStorage from '../../utilities/set-storage';
import constants from '../../constants';

export default async ({ verticalPosition }) =>
  setStorage({
    verticalPosition:
      verticalPosition == constants.POSITIONS.LEFT
        ? constants.POSITIONS.RIGHT
        : constants.POSITIONS.LEFT,
  });
