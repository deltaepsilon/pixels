import setStorage from '../../utilities/set-storage';

export default async (state, dpi) => setStorage({ dpi });
