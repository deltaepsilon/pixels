import setStorage from '../../utilities/set-storage';
import initialState from '../initial-state';

const { horizontalSelected, verticalSelected } = initialState;
const defaultSelected = {
  horizontalSelected,
  verticalSelected,
};

export default async () => setStorage(defaultSelected);
