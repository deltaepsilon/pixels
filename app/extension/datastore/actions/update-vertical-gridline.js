import setStorage from '../../utilities/set-storage';

export default async ({ verticalGridlines }, { index, x }) => {
  const tempGridlines = [...verticalGridlines];

  tempGridlines.splice(index, 1, x);

  return setStorage({
    verticalGridlines: tempGridlines,
  });
};
