import setStorage from '../../utilities/set-storage';

export default async ({ resetCounter }) => setStorage({ resetCounter: resetCounter + 1 });
