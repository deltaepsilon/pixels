import setStorage from '../../utilities/set-storage';
import constants from '../../constants';

export default async ({ localOffsets: existing }, { localId, x, y }) => {
  const localOffsets = { ...existing };
  const tabOffset = localOffsets[localId] || constants.TAB_OFFSET;

  if (typeof x != 'undefined') {
    tabOffset.x = x;
  }

  if (typeof y != 'undefined') {
    tabOffset.y = y;
  }

  localOffsets[localId] = tabOffset;

  return setStorage({ localOffsets });
};
