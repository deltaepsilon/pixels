import setStorage from '../../utilities/set-storage';

export default async (state, horizontalSelected) =>
  setStorage({ horizontalSelected: [...horizontalSelected] });
