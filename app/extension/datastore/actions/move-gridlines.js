import setStorage from '../../utilities/set-storage';

export default async (
  { horizontalGridlines, horizontalSelected, verticalGridlines, verticalSelected },
  { x, y }
) => {
  const changeY = y || 0;
  const changeX = x || 0;
  const updatedHorizontal = horizontalGridlines.map((gridY, i) => {
    if (changeY && horizontalSelected.includes(i)) {
      gridY += changeY;
    }

    return gridY;
  });
  const updatedVertical = verticalGridlines.map((gridX, i) => {
    if (changeX && verticalSelected.includes(i)) {
      gridX += changeX;
    }

    return gridX;
  });

  return setStorage({
    horizontalGridlines: updatedHorizontal,
    verticalGridlines: updatedVertical,
  });
};
