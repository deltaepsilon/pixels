import setStorage from '../../utilities/set-storage';

export default async (state, currentUser) => {
  const { uid } = currentUser || {};

  return setStorage({ currentUser: currentUser ? { uid } : null });
};
