import setStorage from '../../utilities/set-storage';

export default async (state, mouseCoordinates) => setStorage({ mouseCoordinates });
