import setStorage from '../../utilities/set-storage';

export default async ({ windowId, isActiveByWindowId = {} }, isActive) => {
  const update = { ...isActiveByWindowId, [windowId]: isActive };

  return setStorage({ isActive, isActiveByWindowId: update });
};
