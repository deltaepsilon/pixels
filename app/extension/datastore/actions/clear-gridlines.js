import setStorage from '../../utilities/set-storage';
import initialState from '../initial-state';

const {
  horizontalGridlines,
  horizontalSelected,
  verticalGridlines,
  verticalSelected,
} = initialState;
const defaultGridlines = {
  horizontalGridlines,
  horizontalSelected,
  verticalGridlines,
  verticalSelected,
};

export default async () => setStorage(defaultGridlines);
