import setStorage from '../../utilities/set-storage';

export default async (state, gridlineColor) => setStorage({ gridlineColor });
