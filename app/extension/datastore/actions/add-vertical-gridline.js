import utilities from '../../utilities';

const { gridlinesToSelected, setStorage } = utilities;

export default async ({ verticalGridlines, mouseCoordinates, localId, localOffsets }, gridline) => {
  const gridlines = new Set(verticalGridlines);
  const x = gridline || mouseCoordinates.x;
  const offsetX = localOffsets && localOffsets[localId] ? localOffsets[localId].x : 0;
  const offsetAdjustedX = x - offsetX;

  gridlines.add(offsetAdjustedX);

  const unsorted = [...gridlines];
  const sorted = unsorted.sort((a, b) => (a > b ? 1 : -1));
  const selected = gridlinesToSelected([offsetAdjustedX], sorted);

  return setStorage({
    verticalGridlines: sorted,
    verticalSelected: selected,
  });
};
