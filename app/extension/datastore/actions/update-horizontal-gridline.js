import setStorage from '../../utilities/set-storage';

export default async ({ horizontalGridlines }, { index, y }) => {
  const tempGridlines = [...horizontalGridlines];

  tempGridlines.splice(index, 1, y);

  return setStorage({
    horizontalGridlines: tempGridlines,
  });
};
