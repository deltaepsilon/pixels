export default async ({ localId, localWindowId }, localTabId) => {
  const updatedLocalId = localTabId && localWindowId && `${localTabId}-${localWindowId}`;

  return {
    localId: updatedLocalId || localId,
    localTabId,
  };
};
