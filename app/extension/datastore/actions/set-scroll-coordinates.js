import setStorage from '../../utilities/set-storage';

export default async (state, scrollCoordinates) => setStorage({ scrollCoordinates });
