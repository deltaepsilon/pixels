import setStorage from '../../utilities/set-storage';
import initialState from '../initial-state';

export default async ({ localId, tabId, windowId }, overrides = {}) =>
  setStorage({ ...initialState, ...overrides, localId, tabId, windowId });
