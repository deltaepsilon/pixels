import setStorage from '../../utilities/set-storage';

export default async ({ verticalGridlines }, index) => {
  const gridlines = new Set(verticalGridlines);

  gridlines.delete(verticalGridlines[index]);

  return setStorage({ verticalGridlines: [...gridlines] });
};
