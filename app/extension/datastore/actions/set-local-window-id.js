export default async ({ localId, localTabId }, localWindowId) => {
  const updatedLocalId = localTabId && localWindowId && `${localTabId}-${localWindowId}`;

  return {
    localId: updatedLocalId || localId,
    localWindowId,
  };
};
