import setStorage from '../../utilities/set-storage';

export default async (state, verticalSelected) =>
  setStorage({ verticalSelected: [...verticalSelected] });
