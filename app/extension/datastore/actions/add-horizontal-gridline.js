import utilities from '../../utilities';

const { gridlinesToSelected, setStorage } = utilities;

export default async (
  { horizontalGridlines, mouseCoordinates, localId, localOffsets },
  gridline
) => {
  const gridlines = new Set(horizontalGridlines);
  const y = gridline || mouseCoordinates.y;
  const offsetY = localOffsets && localOffsets[localId] ? localOffsets[localId].y : 0;
  const offsetAdjustedY = y - offsetY;

  gridlines.add(offsetAdjustedY);

  const unsorted = [...gridlines];
  const sorted = unsorted.sort((a, b) => (a > b ? 1 : -1));
  const selected = gridlinesToSelected([offsetAdjustedY], sorted);

  return setStorage({
    horizontalGridlines: sorted,
    horizontalSelected: selected,
  });
};
