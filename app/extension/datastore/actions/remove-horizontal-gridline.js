import setStorage from '../../utilities/set-storage';

export default async ({ horizontalGridlines }, index) => {
  const gridlines = new Set(horizontalGridlines);

  gridlines.delete(horizontalGridlines[index]);

  return setStorage({ horizontalGridlines: [...gridlines] });
};
