import setStorage from '../../utilities/set-storage';

export default async ({ isActiveByWindowId = {} }, windowId) => {
  
  let isActive = false;
  
  if (isActiveByWindowId[windowId]) {
    isActive = isActiveByWindowId[windowId];
  }

  return setStorage({ isActive, windowId });
};