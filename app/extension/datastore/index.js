import createStore from 'unistore';
import initialState from './initial-state';

import unboundActions from './actions';

const LOCALSTORAGE_NAME = 'pixels-state';

let localStorageState = getLocalStorage() || {};
const state = { ...initialState, ...localStorageState };

const store = createStore(state);

const actions = store => unboundActions;

setWindowState();
store.subscribe(setWindowState);

function setWindowState() {
  if (typeof window == 'object') {
    window.state = store.getState();
  }
}

store.subscribe(() => setLocalStorage(store.getState()));

function getLocalStorage() {
  let result = {};

  if (typeof window == 'object') {
    const stringified = localStorage.getItem(LOCALSTORAGE_NAME);

    if (stringified) {
      result = JSON.parse(stringified);
    }
  }

  return result;
}

function setLocalStorage(state) {
  localStorage && localStorage.setItem(LOCALSTORAGE_NAME, JSON.stringify(state));
}

export { actions, store };
