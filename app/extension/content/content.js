import React from 'react';
import ReactDom from 'react-dom';
import { Provider, connect } from 'unistore/react';
import { store, actions } from '../datastore';
import SyncStorage from '../components/storage/sync-storage';
import constants from '../constants';
import ReactResizeDetector from 'react-resize-detector';
import CursorTracking from '../components/cursor/cursor-tracking';
import ScrollTracking from '../components/scroll/scroll-tracking';
import HorizontalRuler from '../components/rulers/horizontal-ruler';
import HorizontalControls from '../components/controls/horizontal-controls';
import Gridlines from '../components/gridlines/gridlines';
import VerticalRuler from '../components/rulers/vertical-ruler';
import VerticalControls from '../components/controls/vertical-controls';
import KeyboardControls from '../components/keyboard/keyboard-controls';
import ListenToMessages from '../components/state/listen-to-messages';

import '../../root.css';
import './content.css';

const parentNode = document.documentElement;
const div = getOrCreateDiv();

parentNode.appendChild(div);

observeRemoval(parentNode);

renderIntoParentNode(parentNode);

function getOrCreateDiv() {
  let div = document.getElementById(constants.CONTENT_EL_ID);

  if (!div) {
    div = document.createElement('div');
    div.setAttribute('id', constants.CONTENT_EL_ID);
  }

  return div;
}

function observeRemoval(parentNode) {
  const observer = new MutationObserver(([{ removedNodes }], observer) => {
    const wrapperRemoved = !![...removedNodes].find(el => el.id == constants.CONTENT_EL_ID);

    wrapperRemoved && ReactDom.unmountComponentAtNode(div);
    observer.disconnect();
  });

  parentNode.appendChild(div);
  observer.observe(parentNode, { childList: true });
}

function renderIntoParentNode(parentNode) {
  function Content({ horizontalSelected, verticalSelected, isActive }) {
    const scrollHeight = Math.min(
      parentNode.scrollHeight - constants.DIMENSIONS.RULER_HEIGHT,
      10000
    );
    const scrollWidth = Math.min(parentNode.scrollWidth - constants.DIMENSIONS.RULER_WIDTH, 10000);
    const selected = horizontalSelected.length || verticalSelected.length;

    return isActive ? (
      <div id="__pixels_content__">
        <CursorTracking />
        <ScrollTracking />
        <ListenToMessages />
        <ReactResizeDetector handleWidth handleHeight>
          <Gridlines scrollHeight={scrollHeight} scrollWidth={scrollWidth} />
          <HorizontalRuler />
          <VerticalRuler />
          <VerticalControls />
          <HorizontalControls />
          {selected && <KeyboardControls />}
        </ReactResizeDetector>
      </div>
    ) : null;
  }

  const ConnectedContent = connect(
    'horizontalSelected,verticalSelected,isActive,updated',
    actions
  )(Content);

  ReactDom.render(
    <Provider store={store}>
      <div>
        <SyncStorage />
        <ConnectedContent />
      </div>
    </Provider>,
    document.getElementById(constants.CONTENT_EL_ID)
  );
}
