import getActiveTab from './get-active-tab';
export default async function sendMessageToActiveTab(payload) {
  const { id } = await getActiveTab();

  return new Promise(resolve =>
    chrome.tabs.sendMessage(id, payload, response => resolve(response))
  );
}
