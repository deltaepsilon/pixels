import getCurrentWindow from './get-current-window';
import getActiveTabs from './get-active-tabs';

export default async function getActiveTab() {
  const { id: windowId } = await getCurrentWindow();
  const activeTabs = await getActiveTabs();

  return activeTabs.find(tab => tab.windowId == windowId);
}
