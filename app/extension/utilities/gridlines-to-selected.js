export default function gridlinesToSelected(values, gridlines) {
  return values.map(value => gridlines.findIndex(gridline => gridline == value));
}
