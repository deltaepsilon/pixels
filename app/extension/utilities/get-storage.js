import constants from "../constants";

export default function getStorage(keys = null, namespace = constants.NAMESPACES.LOCAL) {
  return new Promise(resolve => chrome.storage[namespace].get(keys, results => resolve(results)));
}
