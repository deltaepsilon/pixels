export default function selectedToGridlines(selected, gridlines) {
  return gridlines.filter((gridline, i) => selected.includes(i));
}
