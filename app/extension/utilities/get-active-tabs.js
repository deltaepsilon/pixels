export default function getActiveTabs() {
  return new Promise(resolve =>
    chrome.tabs.query({ active: true }, tabs => resolve(tabs))
  );
}
