export default function listenToStorage(cb, requiredNamespace) {
  function listener(changes, namespace) {
    const matchesNamespace = !requiredNamespace || requiredNamespace == namespace;

    matchesNamespace && cb(changes);
  }

  chrome.storage.onChanged.addListener(listener);

  return () => chrome.storage.onChanged.removeListener(listener);
}
