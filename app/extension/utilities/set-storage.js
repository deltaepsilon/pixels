import constants from '../constants';

export default async function setStorage(payload, namespace = constants.NAMESPACES.LOCAL) {
  const state = { ...payload, updated: Date.now() };

  return new Promise(resolve => {
    try {
      return chrome.storage[namespace].set(state, () => resolve(state));
    } catch (e) {
      // console.error('setStorage error, most likely called by an unmounted listener', state, e);
    }
  });
}
