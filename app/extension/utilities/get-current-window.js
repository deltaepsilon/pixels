export default function getActiveTab() {
  return new Promise(resolve => chrome.windows.getCurrent(window => resolve(window)));
}
