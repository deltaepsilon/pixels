export default function sendMessage(payload) {
  return new Promise(resolve => chrome.runtime.sendMessage(payload, response => resolve(response)));
}
