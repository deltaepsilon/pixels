import drawLines from './draw-lines';
import constants from '../constants';
import clearCanvas from './clear-canvas';

export default (
  ctx,
  { gridlineColor, horizontalGridlines, verticalGridlines, localId, localOffsets }
) => {
  const { width, height } = ctx.canvas;
  const offsets = getTabOffset(localId, localOffsets);
  const horizontalLines = getHorizontalLines(width, horizontalGridlines, offsets);
  const verticalLines = getVerticalLines(height, verticalGridlines, offsets);
  const options = { strokeStyle: gridlineColor, lineDashes: [5.5, 3.5], lineWidth: 1 };

  clearCanvas(ctx);
  drawLines(ctx, horizontalLines, options);
  drawLines(ctx, verticalLines, options);
};

function getTabOffset(localId, localOffsets) {
  return localOffsets[localId] || constants.TAB_OFFSET;
}

function getVerticalLines(height, coordinates, offsets) {
  return coordinates
    .map(x => ({
      startX: x + offsets.x,
      endX: x + offsets.x,
      startY: 0,
      endY: height,
    }))
    .map(mapStraddle);
}

function getHorizontalLines(width, coordinates, offsets) {
  return coordinates
    .map(y => ({
      startX: 0,
      endX: width,
      startY: y + offsets.y,
      endY: y + offsets.y,
    }))
    .map(mapStraddle);
}

function mapStraddle({ startX, startY, endX, endY }) {
  return {
    startX: straddle(startX),
    startY: straddle(startY),
    endX: straddle(endX),
    endY: straddle(endY),
  };
}

const dpr = window.devicePixelRatio;
function straddle(px) {
  return Math.ceil(px * dpr - 0.5);
}
