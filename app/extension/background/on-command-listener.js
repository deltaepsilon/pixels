import utilities from '../utilities';
import actions from '../datastore/actions';
import constants from '../constants';

const { getActiveTab, getStorage, setStorage, sendMessage } = utilities;
const { addHorizontalGridline, addVerticalGridline, clearGridlines, setIsActive } = actions;

export default async function(command) {
  await sendMessage({ command });

  const state = await getStorage();

  switch (command) {
    case constants.COMMANDS.TOGGLE_ACTIVE:
      let { isActive } = await getStorage(['isActive']);

      isActive = !isActive;

      await setIsActive(state, isActive);
      break;

    case constants.COMMANDS.CLEAR_GRIDS:
      await clearGridlines();
      break;

    case constants.COMMANDS.ADD_VERTICAL_GRIDLINE:
      await addVerticalGridline(state);
      break;

    case constants.COMMANDS.ADD_HORIZONTAL_GRIDLINE:
      await addHorizontalGridline(state);
      break;

    default:
      console.info('Command:', command);
      break;
  }

  // const updatedState = await getStorage();

  // console.log('updatedState', updatedState.horizontalGridlines);
}
