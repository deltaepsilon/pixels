import constants from '../constants';
import getActiveTab from '../utilities/get-active-tab';

export default async function deactivate() {
  const { id: tabId } = await getActiveTab();

  chrome.pageAction.setIcon({
    tabId,
    path: 'assets/logo-inactive-48.png',
  });

  chrome.tabs.executeScript({
    code: `(function() {
      const el = document.getElementById('${constants.CONTENT_EL_ID}'); 

      el && el.remove();
    })()`,
  });
}
