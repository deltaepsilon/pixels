import syncDom from './sync-dom';
import setTabId from '../datastore/actions/set-tab-id';
import setWindowId from '../datastore/actions/set-window-id';
import getStorage from '../utilities/get-storage';
import sendMessageToActiveTab from '../utilities/send-message-to-active-tab';
import getActiveTab from '../utilities/get-active-tab';

export default async function onFocusChangedListener(windowId) {
  const isDevToolsWindow = windowId == -1;

  if (!isDevToolsWindow) {
    const state = await getStorage();
    const { id: tabId } = await getActiveTab();

    // console.log('on-focus-changed-listener tabId, windowId', tabId, windowId, state);

    await syncDom();
    await setTabId(state, tabId);
    await setWindowId(state, windowId);
    await sendMessageToActiveTab({ localTabId: tabId, localWindowId: windowId });
  }
}
