import syncDom from './sync-dom';
import setTabId from '../datastore/actions/set-tab-id';
import getStorage from '../utilities/get-storage';
import getCurrentWindow from '../utilities/get-current-window';
import sendMessageToActiveTab from '../utilities/send-message-to-active-tab';

export default async function onTabUpdatedListener(tabId, changeInfo, tab) {
  const isComplete = changeInfo.status == 'complete';
  const isDevToolsWindow = tabId == -1;

  if (isComplete && !isDevToolsWindow) {
    const state = await getStorage();
    const { id: windowId } = await getCurrentWindow();

    await syncDom();
    await setTabId(state, tabId);
    await sendMessageToActiveTab({ localTabId: tabId, localWindowId: windowId });
  }
}
