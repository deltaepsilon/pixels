import constants from '../constants';
import getStorage from '../utilities/get-storage';
import sendMessageToActiveTab from '../utilities/send-message-to-active-tab';
import setTabId from '../datastore/actions/set-tab-id';
import setWindowId from '../datastore/actions/set-window-id';
import setStorage from "../utilities/set-storage";

export default async function onActivatedListener({ tabId, windowId }) {
  const state = await getStorage(null, constants.NAMESPACES.SYNC);

  delete state.isActive;
  
  // console.log('on-activated-listener tabId, windowId', tabId, windowId);

  await setTabId(state, tabId);
  await setWindowId(state, windowId);
  await setStorage(state, constants.NAMESPACES.LOCAL);
  sendMessageToActiveTab({ localTabId: tabId, localWindowTab: windowId });
}
