import initialState from '../datastore/initial-state';
import utilities from '../utilities';
import constants from '../constants';
import onActivatedListener from './on-activated-listener';
import onCommandListener from './on-command-listener';
import onTabUpdatedListener from './on-tab-updated-listener';
import onFocusChangedListener from './on-focus-changed-listener';
import syncDom from './sync-dom';

const { debounceAsync, listenToStorage, getStorage, setStorage } = utilities;

chrome.runtime.onInstalled.addListener(async function() {
  // Activate button
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { schemes: ['https', 'http', 'file'] },
          }),
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()],
      },
    ]);
  });

  // Tab activated
  chrome.tabs.onActivated.addListener(onActivatedListener);

  // Tab updated
  chrome.tabs.onUpdated.addListener(onTabUpdatedListener);

  // Window updated
  chrome.windows.onFocusChanged.addListener(onFocusChangedListener);

  // Commands
  chrome.commands.onCommand.addListener(onCommandListener);

  // Storage Changes
  const state = await getStorage();

  await setStorage(state || initialState);

  await syncDom();

  /**
   * Local data updates can happen quickly, but 'sync' updates must be strictly throttled.
   * As a result, we're handling everything locally and copying to 'sync' on a debounced
   * basis.
   *
   * This may turn out to be unnecessary if 'local' and 'sync' work well enough, in which
   * case we can swap 'sync' out for firebase and this logic can copy and read from the
   * RTDB instead.
   */
  const writeLocalToSync = debounceAsync(async () => {
    const state = await getStorage();

    return setStorage(state, constants.NAMESPACES.SYNC);
  }, constants.TIMINGS.SYNC_TIMEOUT);

  listenToStorage(async changes => {
    if (changes.isActive) {
      await syncDom();
    }

    writeLocalToSync();
  }, constants.NAMESPACES.LOCAL);
});
