import utilities from '../utilities';
import activate from './activate';
import deactivate from './deactivate';

const { getStorage } = utilities;

export default async function syncDom() {
  const { isActive } = await getStorage();

  deactivate();

  isActive && activate();
}
