import getActiveTab from '../utilities/get-active-tab';

export default async function activate() {
  const { id: tabId } = await getActiveTab();

  chrome.pageAction.setIcon({
    tabId,
    path: 'assets/logo-48.png',
  });

  chrome.tabs.executeScript({
    file: 'content.js',
  });
}
