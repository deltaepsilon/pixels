import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

class KeyboardControls extends React.Component {
  get event() {
    return 'keydown';
  }

  componentDidMount() {
    this.__handleKey = this.handleKey.bind(this);

    window.addEventListener(this.event, this.__handleKey);
  }

  componentWillUnmount() {
    window.removeEventListener(this.event, this.__handleKey);
  }

  handleKey({ key, shiftKey, altKey }) {
    const { moveGridlines, horizontalSelected, verticalSelected } = this.props;
    const hotKeysPressed = shiftKey && altKey;
    const isSelected = horizontalSelected.length || verticalSelected.length;
    const shouldMoveGridlines = hotKeysPressed && isSelected;

    switch (key) {
      case 'Escape':
        this.props.clearSelected();
        break;
      case 'ArrowUp':
        shouldMoveGridlines && moveGridlines({ y: -1 });
        break;

      case 'ArrowDown':
        shouldMoveGridlines && moveGridlines({ y: 1 });
        break;

      case 'ArrowLeft':
        shouldMoveGridlines && moveGridlines({ x: -1 });
        break;

      case 'ArrowRight':
        shouldMoveGridlines && moveGridlines({ x: 1 });
        break;

      default:
        // console.log('key', key);
        break;
    }
  }

  render() {
    return null;
  }
}

export default connect(
  'horizontalSelected,verticalSelected',
  actions
)(KeyboardControls);
