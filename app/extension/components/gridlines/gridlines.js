import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import renderGridlines from '../../canvas/render-gridlines';

class Gridlines extends React.Component {
  constructor() {
    super();

    this.canvasRef = React.createRef();
  }

  get canvas() {
    return this.canvasRef.current;
  }

  get ctx() {
    return this.canvas && this.canvas.getContext('2d');
  }

  componentDidMount() {
    this.renderCanvas();
  }

  componentDidUpdate() {
    this.renderCanvas();
  }

  renderCanvas() {
    this.ctx && renderGridlines(this.ctx, this.props);
  }

  /**
   * Scale based on window.devicePixelRatio
   *
   * https://medium.com/wdstack/fixing-html5-2d-canvas-blur-8ebe27db07da
   */
  render() {
    const { scrollWidth: width, scrollHeight: height } = this.props;
    const dpr = window.devicePixelRatio;

    return width ? (
      <canvas
        id="__pixels__gridlines"
        style={{ width, height }}
        width={width * dpr}
        height={height * dpr}
        ref={this.canvasRef}
      />
    ) : null;
  }
}

export default connect(
  'gridlineColor,horizontalGridlines,localId,localOffsets,updated,verticalGridlines',
  actions
)(Gridlines);
