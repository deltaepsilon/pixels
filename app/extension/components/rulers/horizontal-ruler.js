import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import renderHorizontalRuler from '../../canvas/render-horizontal-ruler';
import constants from '../../constants';

import './rulers.css';

class HorizontalRuler extends React.Component {
  constructor() {
    super();

    this.canvasRef = React.createRef();
  }

  get canvas() {
    return this.canvasRef.current;
  }

  get ctx() {
    return this.canvas && this.canvas.getContext('2d');
  }

  componentDidMount() {
    this.renderCanvas();
  }

  componentDidUpdate() {
    this.renderCanvas();
  }

  renderCanvas() {
    this.ctx && renderHorizontalRuler(this.ctx, this.props);
  }

  switchPosition() {
    const { horizontalPosition, setHorizontalPosition } = this.props;

    setHorizontalPosition(
      horizontalPosition == constants.POSITIONS.RIGHT
        ? constants.POSITIONS.LEFT
        : constants.POSITIONS.RIGHT
    );
  }

  handleClick(e) {
    this.props.addVerticalGridline();
  }

  /**
   * Scale based on window.devicePixelRatio
   *
   * https://medium.com/wdstack/fixing-html5-2d-canvas-blur-8ebe27db07da
   */
  render() {
    const { width, horizontalPosition, verticalPosition } = this.props;
    const height = constants.DIMENSIONS.RULER_HEIGHT;
    const adjustedWidth = width - height;
    const dpr = window.devicePixelRatio;

    return width ? (
      <canvas
        id="__pixels__horizontal-ruler"
        className={`${horizontalPosition}-${verticalPosition}`}
        style={{ width: adjustedWidth, height }}
        width={adjustedWidth * dpr}
        height={height * dpr}
        ref={this.canvasRef}
        onClick={this.handleClick.bind(this)}
      />
    ) : null;
  }
}

export default connect(
  'dpi,horizontalPosition,verticalPosition',
  actions
)(HorizontalRuler);
