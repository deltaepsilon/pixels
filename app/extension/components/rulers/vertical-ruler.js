import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import renderVerticalRuler from '../../canvas/render-vertical-ruler';
import constants from '../../constants';

import './rulers.css';

class VerticalRuler extends React.Component {
  constructor() {
    super();

    this.canvasRef = React.createRef();
  }

  get canvas() {
    return this.canvasRef.current;
  }

  get ctx() {
    return this.canvas && this.canvas.getContext('2d');
  }

  componentDidMount() {
    this.renderCanvas();
  }

  componentDidUpdate() {
    this.renderCanvas();
  }

  renderCanvas() {
    this.ctx && renderVerticalRuler(this.ctx, this.props);
  }

  switchPosition() {
    const { verticalPosition, setVerticalPosition } = this.props;

    setVerticalPosition(
      verticalPosition == constants.POSITIONS.TOP
        ? constants.POSITIONS.BOTTOM
        : constants.POSITIONS.TOP
    );
  }

  handleClick(e) {
    this.props.addHorizontalGridline();
  }

  /**
   * Scale based on window.devicePixelRatio
   *
   * https://medium.com/wdstack/fixing-html5-2d-canvas-blur-8ebe27db07da
   */
  render() {
    const { height, horizontalPosition, verticalPosition } = this.props;
    const width = constants.DIMENSIONS.RULER_WIDTH;
    const adjustedHeight = height - width;
    const dpr = window.devicePixelRatio;

    return height ? (
      <canvas
        id="__pixels__vertical-ruler"
        className={`${horizontalPosition}-${verticalPosition}`}
        style={{ width, height: adjustedHeight }}
        width={width * dpr}
        height={adjustedHeight * dpr}
        ref={this.canvasRef}
        onClick={this.handleClick.bind(this)}
      />
    ) : null;
  }
}

export default connect(
  'dpi,horizontalPosition,verticalPosition',
  actions
)(VerticalRuler);
