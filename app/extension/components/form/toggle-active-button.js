import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import { IconButton } from '@rmwc/icon-button';
import '@material/icon-button/dist/mdc.icon-button.css';

import './form.css';

export class ToggleActiveButton extends React.Component {
  handleClick() {
    const { isActive, setIsActive } = this.props;

    setIsActive(!isActive);
  }

  render() {
    const { isActive } = this.props;

    return (
      <IconButton
        icon="visibility"
        className={`${isActive ? 'active' : 'inactive'}`}
        onClick={this.handleClick.bind(this)}
      />
    );
  }
}

export default connect(
  'isActive',
  actions
)(ToggleActiveButton);
