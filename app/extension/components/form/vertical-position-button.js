import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import { IconButton } from '@rmwc/icon-button';
import '@material/icon-button/dist/mdc.icon-button.css';

import './form.css';

export class VerticalPositionButton extends React.Component {
  render() {
    const { toggleVerticalPosition } = this.props;

    return <IconButton icon="swap_horiz" onClick={() => toggleVerticalPosition()} />;
  }
}

export default connect(
  '',
  actions
)(VerticalPositionButton);
