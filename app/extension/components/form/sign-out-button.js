/* globals firebase */
import React from 'react';

import { IconButton } from '@rmwc/icon-button';

import '@material/icon-button/dist/mdc.icon-button.css';

export default class SignOutButton extends React.Component {
  get auth() {
    return window.firebase.auth();
  }

  handleClick() {
    return this.auth.signOut();
  }

  render() {
    return (
      <IconButton
        style={{ color: 'var(--color-red)' }}
        icon="power_off"
        onClick={this.handleClick.bind(this)}
      />
    );
  }
}
