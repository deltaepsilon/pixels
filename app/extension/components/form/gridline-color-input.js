import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import { TextField } from '@rmwc/textfield';
import '@material/textfield/dist/mdc.textfield.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';

class GridlineColorInput extends React.Component {
  handleChange({ target }) {
    this.props.setGridlineColor(target.value);
  }

  render() {
    const { gridlineColor } = this.props;

    return (
      <TextField
        label="color"
        value={gridlineColor}
        onChange={this.handleChange.bind(this)}
        type="text"
      />
    );
  }
}

export default connect(
  'gridlineColor',
  actions
)(GridlineColorInput);
