/** globals window */
import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import { IconButton } from '@rmwc/icon-button';
import '@material/icon-button/dist/mdc.icon-button.css';

import './form.css';

export class ResetButton extends React.Component {
  handleClick() {
    const { currentUser, isActive, resetState, incrementResetCounter } = this.props;
    
    resetState({ currentUser, isActive });

    incrementResetCounter();
  }

  render() {
    return <IconButton icon="autorenew" onClick={this.handleClick.bind(this)} />;
  }
}

export default connect(
  'currentUser,isActive',
  actions
)(ResetButton);
