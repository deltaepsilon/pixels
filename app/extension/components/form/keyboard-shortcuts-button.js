import React from 'react';

import { IconButton } from '@rmwc/icon-button';

import '@material/icon-button/dist/mdc.icon-button.css';

export default class KeyboardShortcutsButton extends React.Component {
  handleClick() {
    chrome.tabs.create({ url: 'chrome://extensions/shortcuts' });
  }

  render() {
    return <IconButton icon="keyboard" onClick={this.handleClick.bind(this)} />;
  }
}
