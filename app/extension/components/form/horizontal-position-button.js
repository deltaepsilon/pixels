import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import { IconButton } from '@rmwc/icon-button';
import '@material/icon-button/dist/mdc.icon-button.css';

import './form.css';

export class HorizontalPositionButton extends React.Component {
  render() {
    const { toggleHorizontalPosition } = this.props;

    return <IconButton icon="swap_vert" onClick={() => toggleHorizontalPosition()} />;
  }
}

export default connect(
  '',
  actions
)(HorizontalPositionButton);
