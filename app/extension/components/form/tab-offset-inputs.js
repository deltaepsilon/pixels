import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import constants from '../../constants';

import { TextField } from '@rmwc/textfield';
import '@material/textfield/dist/mdc.textfield.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';

class TabOffsetInputs extends React.Component {
  get tabOffset() {
    const { localId, localOffsets } = this.props;

    return localOffsets[localId] || constants.TAB_OFFSET;
  }

  getChangeHandler(xOrY) {
    return ({ target }) => {
      const { localId } = this.props;

      this.props.setTabOffset({ localId, [xOrY]: +target.value });
    };
  }

  render() {
    const { x, y } = this.tabOffset;

    return (
      <div className="row side-by-side">
        <TextField label="X" value={x} onChange={this.getChangeHandler('x')} type="number" />
        <TextField label="Y" value={y} onChange={this.getChangeHandler('y')} type="number" />
      </div>
    );
  }
}

export default connect(
  'localId,localOffsets',
  actions
)(TabOffsetInputs);
