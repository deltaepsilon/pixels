import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import { TextField, TextFieldIcon, TextFieldHelperText } from '@rmwc/textfield';
import '@material/textfield/dist/mdc.textfield.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';

class DpiInput extends React.Component {
  handleChange({ target }) {
    this.props.setDpi(target.value);
  }

  render() {
    const { dpi } = this.props;

    return (
      <TextField label="dpi" value={dpi} onChange={this.handleChange.bind(this)} type="number" />
    );
  }
}

export default connect(
  'dpi',
  actions
)(DpiInput);
