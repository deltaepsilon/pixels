import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import debounce from '../../utilities/debounce';

class CursorTracking extends React.Component {
  constructor() {
    super();

    this.__handleMouseMove = debounce(this.handleMousemove.bind(this), 50);
  }

  get mousemove() {
    return 'mousemove';
  }

  componentDidMount() {
    window.addEventListener(this.mousemove, this.__handleMouseMove);
  }

  componentWillUnmount() {
    window.removeEventListener(this.mousemove, this.__handleMouseMove);
  }

  handleMousemove(e) {
    const { isActive } = this.props;
    const { pageX, pageY } = e;

    isActive && this.props.setMouseCoordinates({ x: pageX, y: pageY });
  }

  render() {
    return null;
  }
}

export default connect(
  'isActive,updated',
  actions
)(CursorTracking);
