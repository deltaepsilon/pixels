/* globals window */
import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import { Button } from '@rmwc/button';

import '@material/button/dist/mdc.button.css';
import './login.css';

class Login extends React.Component {
  get firebase() {
    return window.firebase;
  }

  get auth() {
    return window.firebase.auth();
  }

  logInWithGoogle(interactive) {
    /**
     * This business is complicated:
     *
     * https://github.com/firebase/quickstart-js/blob/master/auth/chromextension/credentials.js
     */

    chrome.identity.getAuthToken({ interactive: !!interactive }, function(token) {
      if (chrome.runtime.lastError && !interactive) {
        console.info('It was not possible to get a token programmatically.');
      } else if (chrome.runtime.lastError) {
        console.error(chrome.runtime.lastError);
      } else if (token) {
        // Authorize Firebase with the OAuth Access Token.
        const credential = firebase.auth.GoogleAuthProvider.credential(null, token);

        firebase
          .auth()
          .signInAndRetrieveDataWithCredential(credential)
          .catch(function(error) {
            if (error.code === 'auth/invalid-credential') {
              chrome.identity.removeCachedAuthToken({ token: token }, function() {
                this.logInWithGoogle(interactive);
              });
            }
          });
      } else {
        console.error('The OAuth Token was null');
      }
    });
  }

  signInAnonymously() {
    this.auth.signInAnonymously();
  }

  render() {
    return (
      <div id="login">
        <Button raised accent onClick={this.logInWithGoogle.bind(this)}>
          Log in with Google
        </Button>
        <Button raised accent onClick={this.signInAnonymously.bind(this)}>
          Guest Mode
        </Button>
      </div>
    );
  }
}

export default connect(
  'currentUser',
  actions
)(Login);
