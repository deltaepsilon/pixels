import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

import getStorage from '../../utilities/get-storage';
import listenToStorage from '../../utilities/listen-to-storage';

class SyncStorage extends React.Component {
  componentDidMount() {
    this.syncStorage();

    this.__stopListening = listenToStorage(() => this.syncStorage());
  }

  componentWillUnmount() {
    this.__stopListening && this.__stopListening();
  }

  async syncStorage() {
    const { syncStorage } = this.props;

    const state = await getStorage();

    state && state.isActive && syncStorage(state); // prevent memory leak for unmounted components
  }

  render() {
    return null;
  }
}

export default connect(
  '',
  actions
)(SyncStorage);
