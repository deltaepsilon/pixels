import React from 'react';
import { Icon } from '@rmwc/icon';
import '@rmwc/icon/icon.css';

export default ({ onClick }) => {
  return (
    <div className="delete-button" onClick={onClick}>
      🗙
    </div>
  );
};
