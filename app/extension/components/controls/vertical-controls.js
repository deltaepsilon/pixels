import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import constants from '../../constants';
import debounce from '../../utilities/debounce';
import DeleteIcon from './delete-icon';

import './controls.css';

class VerticalControls extends React.Component {
  constructor() {
    super();

    this.__handleMousemove = debounce(this.handleMousemove.bind(this), 10);
    this.__stopDrag = this.stopDrag.bind(this);

    this.state = {
      dragging: false,
      draggingIndex: 0,
      selected: new Set(),
      ignoreNextClick: false,
    };
  }

  get offset() {
    const { localId, localOffsets } = this.props;

    return localOffsets[localId] || constants.TAB_OFFSET;
  }

  componentWillUnmount() {
    this.removeHandlers();
  }

  getMousedownHandler(i) {
    return e => {
      this.setState({ dragging: true, draggingIndex: i });

      window.addEventListener('mousemove', this.__handleMousemove);
      window.addEventListener('mouseup', this.__stopDrag);
      window.addEventListener('mouseleave', this.__stopDrag);
    };
  }

  handleMousemove({ pageY }) {
    const {
      horizontalGridlines,
      horizontalSelected,
      moveGridlines,
      updateHorizontalGridline,
      localId,
      localOffsets,
    } = this.props;
    const { draggingIndex, ignoreNextClick } = this.state;
    const offsetY = localOffsets[localId] ? localOffsets[localId].y : 0;
    const offsetAdjustedMouseY = pageY - offsetY;

    if (horizontalSelected.includes(draggingIndex)) {
      const gridline = horizontalGridlines[draggingIndex];
      const diffY = offsetAdjustedMouseY - gridline;

      moveGridlines({ y: diffY });
    } else {
      updateHorizontalGridline({ index: draggingIndex, y: offsetAdjustedMouseY });
    }

    if (!ignoreNextClick) {
      this.setState({ ignoreNextClick: true });
    }
  }

  stopDrag() {
    this.setState({ dragging: false });
    this.removeHandlers();
  }

  removeHandlers() {
    this.__handleMousemove && window.removeEventListener('mousemove', this.__handleMousemove);
    this.__stopDrag && window.removeEventListener('mouseup', this.__stopDrag);
    this.__stopDrag && window.removeEventListener('mouseleave', this.__stopDrag);
  }

  getClickHandler(i) {
    return e => {
      const { horizontalSelected, setHorizontalSelected } = this.props;
      const { ignoreNextClick } = this.state;
      const selected = new Set(horizontalSelected);

      if (ignoreNextClick) {
        this.setState({ ignoreNextClick: false });
      } else {
        selected.has(i) ? selected.delete(i) : selected.add(i);

        setHorizontalSelected(selected);
      }
    };
  }

  getRemoveHandler(i) {
    return e => this.props.removeHorizontalGridline(i);
  }

  render() {
    const {
      scrollCoordinates,
      horizontalGridlines,
      horizontalSelected,
      verticalPosition,
    } = this.props;
    const { dragging } = this.state;
    const offset = this.offset;
    const x = -1 * scrollCoordinates.x;

    return (
      <div id="__pixels__controls" className={verticalPosition} is-dragging={String(dragging)}>
        {horizontalGridlines.map((y, i) => (
          <Control
            x={x}
            y={y + offset.y}
            verticalPosition={verticalPosition}
            key={i}
            onClick={this.getClickHandler(i)}
            onMouseDown={this.getMousedownHandler(i)}
            handleRemove={this.getRemoveHandler(i)}
            selected={horizontalSelected.includes(i)}
          />
        ))}
      </div>
    );
  }
}

function Control({ x, y, verticalPosition, onClick, handleRemove, onMouseDown, selected }) {
  const style = getStyle({ x, y, verticalPosition });

  return (
    <div
      className="vertical-control control-bar"
      is-selected={String(selected)}
      style={style}
      onClick={onClick}
      onMouseDown={onMouseDown}
    >
      <DeleteIcon onClick={handleRemove} />
    </div>
  );
}

function getStyle({ x, y, verticalPosition }) {
  return verticalPosition == constants.POSITIONS.LEFT ? { top: y, left: -x } : { top: y, right: x };
}

export default connect(
  'horizontalGridlines,horizontalSelected,scrollCoordinates,localId,localOffsets,verticalPosition',
  actions
)(VerticalControls);
