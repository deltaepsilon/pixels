import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import constants from '../../constants';
import debounce from '../../utilities/debounce';
import DeleteIcon from './delete-icon';

import './controls.css';

class HorizontalControls extends React.Component {
  constructor() {
    super();

    this.__handleMousemove = debounce(this.handleMousemove.bind(this), 10);
    this.__stopDrag = this.stopDrag.bind(this);

    this.state = {
      dragging: false,
      draggingIndex: 0,
      selected: new Set(),
      ignoreNextClick: false,
    };
  }

  get offset() {
    const { localId, localOffsets } = this.props;

    return localOffsets[localId] || constants.TAB_OFFSET;
  }

  componentWillUnmount() {
    this.removeHandlers();
  }

  getMousedownHandler(i) {
    return e => {
      this.setState({ dragging: true, draggingIndex: i });

      window.addEventListener('mousemove', this.__handleMousemove);
      window.addEventListener('mouseup', this.__stopDrag);
      window.addEventListener('mouseleave', this.__stopDrag);
    };
  }

  handleMousemove({ pageX }) {
    const {
      verticalGridlines,
      verticalSelected,
      moveGridlines,
      updateVerticalGridline,
      localId,
      localOffsets,
    } = this.props;
    const { draggingIndex, ignoreNextClick } = this.state;
    const offsetX = localOffsets[localId] ? localOffsets[localId].x : 0;
    const offsetAdjustedMouseX = pageX - offsetX;

    if (verticalSelected.includes(draggingIndex)) {
      const gridline = verticalGridlines[draggingIndex];
      const diffX = offsetAdjustedMouseX - gridline;

      moveGridlines({ x: diffX });
    } else {
      updateVerticalGridline({ index: draggingIndex, x: offsetAdjustedMouseX });
    }

    if (!ignoreNextClick) {
      this.setState({ ignoreNextClick: true });
    }
  }

  stopDrag() {
    // console.log('this.props.verticalSelected', this.props.verticalSelected);
    // console.log('this.state.draggingIndex', this.state.draggingIndex);
    this.setState({ dragging: false });

    this.removeHandlers();
  }

  removeHandlers() {
    this.__handleMousemove && window.removeEventListener('mousemove', this.__handleMousemove);
    this.__stopDrag && window.removeEventListener('mouseup', this.__stopDrag);
    this.__stopDrag && window.removeEventListener('mouseleave', this.__stopDrag);
  }

  getClickHandler(i) {
    return e => {
      const { verticalSelected, setVerticalSelected } = this.props;
      const { ignoreNextClick } = this.state;
      const selected = new Set(verticalSelected);

      if (ignoreNextClick) {
        this.setState({ ignoreNextClick: false });
      } else {
        selected.has(i) ? selected.delete(i) : selected.add(i);

        setVerticalSelected(selected);
      }
    };
  }

  getRemoveHandler(i) {
    return e => this.props.removeVerticalGridline(i);
  }

  render() {
    const {
      horizontalPosition,
      scrollCoordinates,
      verticalGridlines,
      verticalSelected,
    } = this.props;
    const { dragging } = this.state;
    const offset = this.offset;
    const y = -1 * scrollCoordinates.y;

    return (
      <div id="__pixels__controls" className={horizontalPosition} is-dragging={String(dragging)}>
        {verticalGridlines.map((x, i) => (
          <Control
            x={x + offset.x}
            y={y}
            horizontalPosition={horizontalPosition}
            key={i}
            onClick={this.getClickHandler(i)}
            onMouseDown={this.getMousedownHandler(i)}
            handleRemove={this.getRemoveHandler(i)}
            selected={verticalSelected.includes(i)}
          />
        ))}
      </div>
    );
  }
}

function Control({ x, y, horizontalPosition, onClick, handleRemove, onMouseDown, selected }) {
  const style = getStyle({ x, y, horizontalPosition });

  return (
    <div
      className="horizontal-control control-bar"
      is-selected={String(selected)}
      style={style}
      onClick={onClick}
      onMouseDown={onMouseDown}
    >
      <DeleteIcon onClick={handleRemove} />
    </div>
  );
}

function getStyle({ x, y, horizontalPosition }) {
  return horizontalPosition == constants.POSITIONS.BOTTOM
    ? { bottom: y, left: x }
    : { top: -y, left: x };
}

export default connect(
  'horizontalPosition,scrollCoordinates,verticalGridlines,verticalSelected,localId,localOffsets',
  actions
)(HorizontalControls);
