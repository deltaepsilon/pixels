import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import getActiveTab from '../../utilities/get-active-tab';

class UpdateLocalTabId extends React.Component {
  componentDidMount() {
    this.syncLocalTabId();
  }

  async syncLocalTabId() {
    const { id } = await getActiveTab();

    this.props.setLocalTabId(id);
  }

  render() {
    return null;
  }
}

export default connect(
  '',
  actions
)(UpdateLocalTabId);
