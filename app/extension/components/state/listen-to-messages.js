import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';

class ListenToMessages extends React.Component {
  constructor() {
    super();

    this.__handleMessage = this.handleMessage.bind(this);
  }

  componentDidMount() {
    chrome.runtime.onMessage.addListener(this.__handleMessage);
  }

  componentWillMount() {
    chrome.runtime.onMessage.removeListener(this.__handleMessage);
  }

  handleMessage({ localTabId, localWindowId }, sender, sendResponse) {
    if (localTabId) {
      this.props.setLocalTabId(localTabId);
    }

    if (localWindowId) {
      this.props.setLocalWindowId(localWindowId);
    }
  }

  render() {
    return null;
  }
}

export default connect(
  '',
  actions
)(ListenToMessages);
