import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import getCurrentWindow from '../../utilities/get-current-window';

class UpdateLocalWindowId extends React.Component {
  componentDidMount() {
    this.syncLocalWindowId();
  }

  async syncLocalWindowId() {
    const { id } = await getCurrentWindow();

    this.props.setLocalWindowId(id);
  }

  render() {
    return null;
  }
}

export default connect(
  '',
  actions
)(UpdateLocalWindowId);
