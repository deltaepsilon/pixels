import React from 'react';
import { connect } from 'unistore/react';
import { actions } from '../../datastore';
import debounce from '../../utilities/debounce';

class ScrollTracking extends React.Component {
  constructor() {
    super();

    this.__handleScroll = debounce(this.handleScroll.bind(this), 50);
  }

  get event() {
    return 'scroll';
  }

  componentDidMount() {
    this.handleScroll();

    window.addEventListener(this.event, this.__handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener(this.event, this.__handleScroll);
  }

  componentDidUpdate(prevProps) {
    prevProps.resetCounter != this.props.resetCounter && this.handleScroll();
  }

  /**
   * Mouse coordinates can only be captured on mousemove, so if a user scrolls without moving
   * the mouse, we need to track the original, "unshifted" mouse coordinates and increment them
   * by the scrollX and scrollY
   */
  handleScroll() {
    const { isActive, mouseCoordinates } = this.props;
    const { scrollX, scrollY } = window;
    const unshifted = mouseCoordinates.unshifted || mouseCoordinates;
    const shiftedMouseCoordinates = {
      x: unshifted.x + scrollX,
      y: unshifted.y + scrollY,
      unshifted: unshifted,
    };

    isActive && this.props.setScrollCoordinates({ x: scrollX, y: scrollY });
    isActive && this.props.setMouseCoordinates(shiftedMouseCoordinates);
  }

  render() {
    return null;
  }
}

export default connect(
  'isActive,mouseCoordinates,updated,resetCounter',
  actions
)(ScrollTracking);
