/* globals window */
import React from 'react';

import DpiInput from '../form/dpi-input';
import GridlineColorInput from '../form/gridline-color-input';
import HorizontalPositionButton from '../form/horizontal-position-button';
import KeyboardShortcutsButton from '../form/keyboard-shortcuts-button';
import ResetButton from '../form/reset-button';
import SignOutButton from '../form/sign-out-button';
import TabOffsetInputs from '../form/tab-offset-inputs';
import ToggleActiveButton from '../form/toggle-active-button';
import VerticalPositionButton from '../form/vertical-position-button';

import './dashboard.css';

export default props => {
  return (
    <div id="dashboard">
      <form>
        <div className="row thin-top thin-bottom">
          <h3 className="thin-top">Rulers</h3>
          <DpiInput />
          <div className="row thin-bottom">
            <HorizontalPositionButton />
            <VerticalPositionButton />
          </div>
        </div>

        <div className="row">
          <h3>Gridlines</h3>
          <GridlineColorInput />
        </div>

        <div className="row">
          <h3>Tab Offsets</h3>
          <TabOffsetInputs />
        </div>

        <div className="row thin-bottom">
          <ToggleActiveButton />
          <ResetButton />
          <KeyboardShortcutsButton />
          <SignOutButton />
        </div>
      </form>
    </div>
  );
};

function signOut() {
  return window.firebase.auth().signOut();
}
