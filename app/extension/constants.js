export const COLORS = {
  RULER_TICK: 'black',
  GUIDELINE: '#004c8c',
};

export const CONTENT_EL_ID = '__pixels_wrapper__';

export const COMMANDS = {
  TOGGLE_ACTIVE: '01-toggle-active',
  CLEAR_GRIDS: '02-clear-grids',
  ADD_VERTICAL_GRIDLINE: '03-add-vertical-gridline',
  ADD_HORIZONTAL_GRIDLINE: '04-add-horizontal-gridline',
  CLEAR_SELECTED_GRIDLINES: '05-clear-selected-gridlines',
  MOVE_UP: '06-move-up',
  MOVE_DOWN: '06-move-down',
};

export const DIMENSIONS = {
  RULER_WIDTH: 20,
  RULER_HEIGHT: 20,
};

export const NAMESPACES = {
  LOCAL: 'local',
  SYNC: 'sync',
};

export const POSITIONS = {
  TOP: 'top',
  RIGHT: 'right',
  BOTTOM: 'bottom',
  LEFT: 'left',
};

export const TAB_OFFSET = {
  x: 0,
  y: 0,
};

export const TIMINGS = {
  SYNC_TIMEOUT: 0.5 * 1000,
};

export default {
  COLORS,
  CONTENT_EL_ID,
  COMMANDS,
  DIMENSIONS,
  NAMESPACES,
  POSITIONS,
  TAB_OFFSET,
  TIMINGS,
};
