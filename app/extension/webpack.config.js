const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ChromeExtensionReloader = require('webpack-chrome-extension-reloader');
const ReplaceInFilePlugin = require('replace-in-file-webpack-plugin');
const { DEV_OAUTH_CLIENT_ID, PROD_OAUTH_CLIENT_ID, OAUTH_CLIENT_ID } = process.env;
const oauthClientId = PROD_OAUTH_CLIENT_ID || DEV_OAUTH_CLIENT_ID || OAUTH_CLIENT_ID;
const { version } = require('../package.json');

module.exports = {
  entry: {
    popup: './extension/popup/popup.js',
    background: './extension/background/background.js',
    content: './extension/content/content.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/, /background\.js/],
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new ChromeExtensionReloader({
      reloadPage: true,
      entries: {
        contentScript: 'popup',
        background: 'background',
        content: 'content',
      },
    }),
    new CopyWebpackPlugin([
      { from: './extension/popup/popup.html', to: 'popup.html' },
      { from: './extension/manifest.json', to: 'manifest.json' },
      { from: './assets/public', to: 'assets' },
    ]),
    new ReplaceInFilePlugin([
      {
        dir: './extension/dist',
        files: ['manifest.json'],
        rules: [
          {
            search: 'ENV_OAUTH_CLIENT_ID',
            replace: oauthClientId,
          },
          {
            search: 'ENV_VERSION',
            replace: version,
          },
        ],
      },
    ])
  ],
};
