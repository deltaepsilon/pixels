import React from 'react';
import ReactDom from 'react-dom';
import { Provider, connect } from 'unistore/react';
import { actions, store } from '../datastore';

import Dashboard from '../components/dashboard/dashboard';
import Login from '../components/login/login';
import Authentication from '../components/authentication/authentication';
import SyncStorage from '../components/storage/sync-storage';
import UpdateLocalTabId from '../components/state/update-local-tab-id';
import UpdateLocalWindowId from '../components/state/update-local-window-id';

import '../../root.css';
import './popup.css';

function Popup({ currentUser }) {
  return (
    <div>
      <Authentication />
      <SyncStorage />
      <UpdateLocalTabId />
      <UpdateLocalWindowId />
      {currentUser && <Dashboard />}
      {!currentUser && <Login />}
    </div>
  );
}

const ConnectedPopup = connect(
  'currentUser',
  actions
)(Popup);

ReactDom.render(
  <Provider store={store}>
    <ConnectedPopup />
  </Provider>,
  document.getElementById('root')
);
